<?php
	require('dbcon.php');
	session_start();
	$email = $_SESSION['temp_email'];
	$phone = $_SESSION['phone'];
	$dob = $_SESSION['dob'];
	$new_pass = $_POST['new_pass'];
	$con_pass = $_POST['con_pass'];
	$hashed_pass = hash('adler32',$new_pass);
	if(!isset($_SESSION['temp_email']) || !isset($_SESSION['phone']) || !isset($_SESSION['dob']))
	{
		header("refresh:0;url=forgot-password.html");
		die();
	}
	
	if($new_pass != $con_pass)
	{
		echo '<script>alert("Passwords do not match")</script>';
		//header("refresh:0;url=../forgotpassword3.php");
		echo $new_pass, $con_pass;
		die();
	}
	else
	{
		$sql = "SELECT * FROM users where `email`='$email' and `phone`='$phone' and `dob`='$dob'";
		$result = mysqli_query($con,$sql);
		$count = mysqli_num_rows($result);
																									
		if($count<=0)
		{
			echo '<script>alert("Account not found!")</script>';
			header("refresh:0;url=../forgot-password.html");
			die();
		}
		else
			{
				$pass_sql = "UPDATE `users` SET  `password`='$hashed_pass' where `email`='$email' and `phone`='$phone' and `dob`='$dob'";
				$pass_result = mysqli_query($con,$pass_sql);
				
				if(!$pass_result)
				{
					echo '<script>alert("Failed to reset password!")</script>';
					header("refresh:0;url=../forgot-password.html");
					die();
				}
				else
				{
					echo '<script>alert("Password reset succefully!")</script>';
					header("refresh:0;url=../login.html");
					die();
				}
			}
	}
?>