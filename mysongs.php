<?php
	session_start();
	if (isset($_SESSION['name'])) 
	{
	$name = $_SESSION['name'];
	$id = $_SESSION['id'];
	require('model/dbcon.php');
	require('model/showpropic.php');
	}
	else
	{
		echo '<script>alert("Login in first")</script>';
    header("refresh:0;url=login.html");
}
?>	
	
<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>My Songs - Groovify</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=ABeeZee">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Aladin">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alef">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allan">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Allura">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/css/styles.min.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/animate.css/3.5.2/animate.min.css">
	
</head>

<body>
    <div id="wrapper">
        <nav class="navbar navbar-light border rounded align-items-start sidebar sidebar-dark accordion bg-gradient-primary p-0" style="background-color: rgb(116,53,195);">
            <div class="container-fluid d-flex flex-column p-0">
                <a class="navbar-brand d-flex justify-content-center align-items-center jello animated sidebar-brand m-0" href="#">
                    <div class="sidebar-brand-icon rotate-n-15"><i class="fas fa-headphones-alt"></i></div>
                    <div class="sidebar-brand-text mx-3"><span>Groovify</span></div>
                </a>
                <hr class="sidebar-divider my-0">
                <ul class="nav navbar-nav text-light" id="accordionSidebar">
                    <li class="nav-item" role="presentation"><a class="nav-link" href="profile.php"><i class="fas fa-user"></i><span>Profile</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="artistview.php"><i class="fas fa-search"></i><span>Browse Songs</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="mysongs.php"><i class="fas fa-music"></i><span>My Songs</span></a></li>
                    <li class="nav-item" role="presentation"><a class="nav-link" href="upload.php"><i class="fas fa-cloud-upload-alt"></i><span>Upload New Song</span></a></li>
					<li class="nav-item" role="presentation"><a class="nav-link" href="chat.php"><i class="fas fa-paper-plane"></i><span>Messages</span></a></li>
                </ul>
                <div class="text-center d-none d-md-inline"><button class="btn rounded-circle border-0" id="sidebarToggle" type="button"></button></div>
            </div>
        </nav>
        <div class="d-flex flex-column" id="content-wrapper">
            <div id="content" style="background-image: url(&quot;assets/img/christina-rumpf--yPHGDPByG0-unsplash.jpg&quot;);">
                <nav class="navbar navbar-light navbar-expand bg-white shadow mb-4 topbar static-top">
                    <div class="container-fluid"><button class="btn btn-link d-md-none rounded-circle mr-3" id="sidebarToggleTop" type="button"><i class="fas fa-bars"></i></button><input type="search" style="width: 266px;"><button class="btn btn-primary" type="button" style="margin-left: 12px;">Search</button>
                        <ul
                            class="nav navbar-nav flex-nowrap ml-auto">
                            <li class="nav-item dropdown d-sm-none no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-search"></i></a>
                                <div class="dropdown-menu dropdown-menu-right p-3 animated--grow-in" role="menu" aria-labelledby="searchDropdown">
                                    <form class="form-inline mr-auto navbar-search w-100">
                                        <div class="input-group"><input class="bg-light form-control border-0 small" type="text" placeholder="Search for ...">
                                            <div class="input-group-append"><button class="btn btn-primary py-0" type="button"><i class="fas fa-search"></i></button></div>
                                        </div>
                                    </form>
                                </div>
                            </li>
                            <li class="nav-item dropdown no-arrow mx-1" role="presentation">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><i class="fas fa-envelope fa-fw"></i><span class="badge badge-danger badge-counter">7</span></a>
                                    <div class="dropdown-menu dropdown-menu-right dropdown-list dropdown-menu-right animated--grow-in"
                                        role="menu">
                                        <h6 class="dropdown-header">alerts center</h6>
                                        <?php
											$sql = "SELECT * FROM chat where `receiver`='$id' ORDER BY `msg_id` DESC";
											$result = mysqli_query($con,$sql);
											$count = mysqli_num_rows($result);
																								
											if($count<=0)
											{
												echo "<td>No Messages found</td>";
											}
											else
											{
												while ($row = mysqli_fetch_array($result))
												{
													
													$m_id = $row['msg_id'];
													$sender = $row['sender'];
													$date = $row['date'];
													$time = $row['time'];
													$message = $row['message'];
													$status = $row['status'];
													
													
													$song_search = "SELECT * FROM users WHERE `id`='$sender'";
													$song_search_result = mysqli_query($con,$song_search);
													while ($row = mysqli_fetch_array($song_search_result))
																	{
																		$artist_name = $row['artist_name'];
																		$propic = $row['profilepic'];
																		$path = "model/profilepics/".$propic;
																	}
													
													if($status==0)
													{
													echo "
														<a class='d-flex align-items-center dropdown-item' href='viewmsg.php?msgid=$m_id&st=in'>
															<div class='dropdown-list-image mr-3'><img class='rounded-circle' src='$path'>
																<div class='bg-success status-indicator'></div>
															</div>
															<div class='font-weight-bold'>
																<div class='text-truncate'><span>$message</span></div>
																<p class='small text-gray-500 mb-0'>$artist_name - $date $time</p>
															</div>
														</a>
													";
													}
													else
													{
														echo "
														<a class='d-flex align-items-center dropdown-item' href='viewmsg.php?msgid=$m_id&st=in'>
															<div class='dropdown-list-image mr-3'><img class='rounded-circle' src='$path'>
																<div class='status-indicator'></div>
															</div>
															<div >
																<div class='text-truncate'><span>$message</span></div>
																<p class='small text-gray-500 mb-0'>$artist_name - $date $time</p>
															</div>
														</a>";
													$m_id = 5;
													}
													
												}
												
											}
										?>
										<a class="text-center dropdown-item small text-gray-500" href="chat.php">Show All Alerts</a></div>
                                </div>
                                <div class="shadow dropdown-list dropdown-menu dropdown-menu-right" aria-labelledby="alertsDropdown"></div>
                            </li>
                            <div class="d-none d-sm-block topbar-divider"></div>
                            <li class="nav-item dropdown no-arrow" role="presentation">
                                <div class="nav-item dropdown no-arrow"><a class="dropdown-toggle nav-link" data-toggle="dropdown" aria-expanded="false" href="#"><span class="d-none d-lg-inline mr-2 text-gray-600 small"><?php echo $_SESSION['name'];?></span><img class="border rounded-circle img-profile" <?php echo "src='model/profilepics/$profilepic'"?>></a>
                                    <div
                                        class="dropdown-menu shadow dropdown-menu-right animated--grow-in" role="menu"><a class="dropdown-item" role="presentation" href="profile.html"><i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Profile</a>
                                        <a
                                            class="dropdown-item" role="presentation" href="activitylog.php"><i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Activity log</a>
                                            <div class="dropdown-divider"></div><a class="dropdown-item" role="presentation" href="model/logout.php"><i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>&nbsp;Logout</a></div>
                    </div>
                    </li>
                    </ul>
            </div>
            </nav>
            <div class="container-fluid" style="height: 123px;">
                <h3 class="text-dark mb-1" style="font-size: 34px;">My Songs</h3>
                <div style="height: 649px;">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr></tr>
                            </thead>
                            <tbody>
                                <?php
								$sql = "SELECT * FROM songs where user_id='$id'";
										$result = mysqli_query($con,$sql);
										$count = mysqli_num_rows($result);
																							
										if($count<=0)
										{
											echo "No songs available";
										}
										else
										{
											while ($row = mysqli_fetch_array($result))
											{
											$song_name = $row['name'];
											$file_name = $row['filename'];
											$song_id = $row['id'];
											$path = "model/songs/".$file_name;
											echo "<tr>
												<script>
													function myFunction() {
													  var r = confirm('Are you sure you want to delete this song');
													  if (r == true) {
														window.location.href = 'model/deletesong.php?song_id=$song_id';
													  } 

													}
												</script>
												<td class='bounce animated' style='height: 20px;width: 300px;'><span>$song_name</span></td>
												<td style='height: 34px;'><audio controls='' controlsList='nodownload' style='height: 26px;opacity: 1;'><source type='audio/mp3' src='$path'><source type='audio/mpeg' src='$path'></audio>
												&nbsp &nbsp &nbsp &nbsp &nbsp <a href='edit.php?song_id=$song_id'><button>Edit</button></a>&nbsp &nbsp<button onclick='myFunction()'>Delete</button></td>
												
											</tr>";
											echo "
												
											";
											}
										}
                                ?>
                            </tbody>
                        </table>
						
                    </div>
                </div>
            </div>
        </div>
        <footer class="bg-white sticky-footer">
            <div class="container my-auto">
                <div class="text-center my-auto copyright"><span>Copyright © Groovify 2021</span></div>
            </div>
        </footer>
    </div><a class="border rounded d-inline scroll-to-top" href="#page-top"><i class="fas fa-angle-up"></i></a></div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-easing/1.4.1/jquery.easing.js"></script>
    <script src="assets/js/script.min.js"></script>
</body>

</html>